/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import implementation.EventManager;
import implementation.PubSubAgent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author Azielio
 */
public class Main {

    public static void main(String[] args) throws UnknownHostException, IOException {
        System.err.println(InetAddress.getLocalHost());
        System.err.println(InetAddress.getLocalHost().getHostAddress());
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter s for server, c for client, anything else to quit.");
        String input = scan.nextLine();
        if (input.equalsIgnoreCase("s")) {
//            EventManager e = new EventManager();
//            e.main_method(args);
            new EventManager().startService();
        } else if (input.equalsIgnoreCase("c")) {
            System.out.println("What's the Host/IP Address of the server you're trying to connect to?");
            input = scan.nextLine();
            PubSubAgent psa = new PubSubAgent();
            psa.main_method(input);
        } else {
            System.out.println("Quitting...");
            System.exit(0);
        }
    }
}
