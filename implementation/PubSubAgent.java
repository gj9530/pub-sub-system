package implementation;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import template.Event;
import template.Publisher;
import template.Subscriber;
import template.Topic;

public class PubSubAgent implements Publisher, Subscriber {

    static String ip = "127.0.0.1";
    static int port = 5000;
    static int assignedPort;
    static int receiverPort;
    static boolean running = true;
    static String text;
    static String line;
    static String username;
    static int identity;
    static ObjectOutputStream out_obj;
    static ObjectInputStream in_obj;
    static DataOutputStream output;
    static DataInputStream input;
    static BufferedReader typedIn;
    static Socket clientSocket;
    static Socket serviceSocket;
    static LinkedHashMap<Integer, Topic> topicIDs;
	private ServerSocket receiverSocket;

    @Override
    public void subscribe(Topic topic) {
    	int identity = 2; //subscriber
    	int request = 1; // subscribe
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setTopic(topic); 	
    	agentData.setUsername(username);
        try {
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);
            out_obj.flush();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	

    }

    @Override
    public void subscribe(String keyword) {
    }

    @Override
    public void unsubscribe(Topic topic) {
    	int identity = 2; //subscriber
    	int request = 2; // subscribe
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setTopic(topic); 	
    	agentData.setUsername(username);
        try {
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);
            out_obj.flush();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unsubscribe() {
    	int identity = 2; //subscriber
    	int request = 5; // unsubscribe all the topics
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setUsername(username);
        try {
        	clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);
            out_obj.flush();
            clientSocket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void listSubscribedTopics() {
    	int identity = 2; //subscriber
    	int request = 4; // list subscribed topics
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setUsername(username);
        try {
        	clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);        
            out_obj.flush();
            ArrayList<Topic> subscribedTopics = new ArrayList<>();
            subscribedTopics = (ArrayList<Topic>) in_obj.readObject();
            if (subscribedTopics.size() == 0) {
                System.out.println("No subscription");
            } 
            else {
                for (Topic t : subscribedTopics) {
                    System.out.println(t.getId() + "  " + t.getName());
                }
            }          
            clientSocket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publish(Event event) {
    	int identity = 1; //publisher
    	int request = 1; // publish
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setEvent(event); 	
        try {
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);
            out_obj.flush();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * This method send the new topic object to the server.
     */
    @Override
    public void advertise(Topic newTopic) {
    	int identity = 1; //publisher
    	int request = 2; // advertise
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setTopic(newTopic);
        try {
            clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            for(int id : topicIDs.keySet()){
            	Topic t = topicIDs.get(id);
            	if(newTopic.getName().equals(t.getName())){
            		System.out.println("This topic has been advertised!");
            		return;
            	}
            }
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);
            out_obj.flush();
            clientSocket.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void processSubscriber() {
    	Receiver r = new Receiver();
		Thread t = new Thread(r);
		t.start();
		
        System.out.println("I am a subscriber");
        System.out.println("Enter your username");
        try {
            line = typedIn.readLine();
            username = line;
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        processUserInput();
        
    }

    private void processUserInput() {
    	boolean b = true;
    	while(b){
    		System.out.println("=====================================");
    		System.out.print("Enter 1 ~ 6:\n 1.subscribe;\n 2.unsubscribe;\n "
                    + "3.subscribe based on keywords;\n 4.list subscribed topics;");
            System.out.println("\n 5.unsubscribe all;\n 6.list all the topics;\n 7. check notifications; \n 8. exit");
            System.out.println("=====================================");
            try {
                line = typedIn.readLine();
                if (line.equals("1")) {
                    System.out.println("Wants to subscribe a topic");
                    toSubscribe();
                } else if (line.equals("2")) {
                    toUnsubscribe();
                } else if (line.equals("3")) {
                    subscribe_keywords();
                } else if (line.equals("4")) {
                    listSubscribedTopics();
                } else if (line.equals("5")) {
                    unsubscribe();
                } else if (line.equals("6")) {
                    toShowTopics();
                } else if (line.equals("7")) {
                    checkEvent();
                } else if (line.equals("8")){
                	receiverSocket.close();
                	b = false;
                } else {
                    System.out.println("Invalid request.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
    		
    	}
        
    }

    private void checkEvent() {
    	int identity = 2; //subscriber
    	int request = 7; // check notifications
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setUsername(username);
        try {
        	clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);
            out_obj.flush();
            ArrayList<Event> events = new ArrayList<>();
            events = (ArrayList<Event>) in_obj.readObject();
            if(events.size() != 0){
            	System.out.println("A new notification received");
            	for(Event event:events){          		
    				System.out.println("Event ID: " + event.getId());
    				System.out.println("Topic: " + event.getTopic().getName());
    				System.out.println("Title: " + event.getTitle() );
    				System.out.println("Content: " + event.getContent());		
            	}
            } else {
            	System.out.println("No new notification received!");
            }
            clientSocket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        
    }

    //Print out all the topics
    private void toShowTopics() throws ClassNotFoundException, IOException {
        clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
        in_obj = new ObjectInputStream(clientSocket.getInputStream());
        topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject(); 	
        System.out.println("ID" + "\t" + "Name");
        for (int topicId : topicIDs.keySet()) {
            System.out.println(topicId + "\t" + topicIDs.get(topicId).getName());
        }

    }

    private void toSubscribe() {
        try {
            System.out.println("Enter the topic ID you want to subscribe");
            line = typedIn.readLine(); //enter the topic id to subscribe
            boolean canProcess = isInt(line);
            if (!canProcess) {
                System.out.println("The topic id must be an integer");
                return;
            }
            int id = Integer.valueOf(line);
            clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            if (!topicIDs.containsKey(id)) {
                System.out.println("This topic id doesn't exist");
                return;
            }
            Topic t =  topicIDs.get(id);
            subscribe(t);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void subscribe_keywords() throws IOException {
        System.out.println("Enter the keyword");
        line = typedIn.readLine();
        int identity = 2; //publisher
    	int request = 3; // publish
    	AgentData agentData  = new AgentData();
    	agentData.setIdentity(identity);
    	agentData.setRequest(request);
    	agentData.setKeywords(line);
    	agentData.setUsername(username);
        try {
        	clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            out_obj = new ObjectOutputStream(clientSocket.getOutputStream());
            out_obj.flush();
            out_obj.writeObject(agentData);
            out_obj.flush();
            clientSocket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void toUnsubscribe() {
        try {
            System.out.println("Enter the topic ID you want to unsubscribe");
            line = typedIn.readLine(); //enter the topic id to subscribe
            boolean canProcess = isInt(line);
            if (!canProcess) {
                System.out.println("The topic id must be an integer");
                return;
            }
            int id = Integer.valueOf(line);
            clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            Topic t = topicIDs.get(id);
            unsubscribe(t);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void processPublisher() {
        System.out.println("I am a publisher");
        boolean b = true;    
        while(b){
        	System.out.println("Enter 1 to publish; 2 to advertise; 3 to list all the topics; 4 to exit");
            try {
                line = typedIn.readLine();
                if (line.equals("1")) {
                    System.out.println("Wants to publish an Event");
                    toPublish();
                } else if (line.equals("2")) {
                    toAdvertise();
                } else if (line.equals("3")) {
                    System.out.println("List all the topics");
                    toShowTopics();
                } else if (line.equals("4")) {
                    b = false;
                } else {
                    System.out.println("Invalid request.");
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        	
        }
        

        
    }

    /*
     * this method creates an Event object
     */
    private void toPublish() {
        try {
            boolean canProcess;
            System.out.println("Enter Event topic ID/title");
            line = typedIn.readLine(); // e.g if topic = 1 title=I love football, type 1/I love football
            canProcess = line.split("/").length == 2;
            if (!canProcess) {
                System.out.println("Wrong command");
                return;
            }
            canProcess = isInt(line.split("/")[0]);
            if (!canProcess) {
                System.out.println("The topic ID is not an integer");
                return;
            }
            int eventTopicId = Integer.valueOf(line.split("/")[0]); //event topic id
            String eventTitle = line.split("/")[1]; //title   		
            System.out.println("Please enter content. Type -end in a new line at the end of the content.");
            StringBuilder sb = new StringBuilder();
            boolean end = false;
            while (!end) {
                line = typedIn.readLine();
                if (line.equals("-end")) {
                    end = true;
                } else {
                    sb.append(line + "\n");
                }
            }
            String content = sb.toString();
            clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
            System.out.println("connect to assigned port of " + clientSocket.getPort());
            in_obj = new ObjectInputStream(clientSocket.getInputStream());
            topicIDs = (LinkedHashMap<Integer, Topic>) in_obj.readObject();
            if (!topicIDs.containsKey(eventTopicId)){
            	System.out.println("The topic doesn't exist!");
            	return;
            }
            Topic eventTopic = topicIDs.get(eventTopicId);
            
			Event event = new Event(0, eventTopic, eventTitle, content); //create an event object
			System.out.println("In p/s class, event = " + event.getContent());
            publish(event);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void toAdvertise() {
        try {
            boolean canProcess;
            System.out.println("Enter topic name and keywords (name/keywords).");
            System.out.println("The keywords are separated by spaces");
            line = typedIn.readLine(); // e.g if id=1 name=topic1 keywords=a b c, type 1/topic1/a b c
            canProcess = line.split("/").length == 2;
            if (!canProcess) {
                System.out.println("Wrong command!");
                return;
            }

            int id = 0;
            String name = line.split("/")[0];
            String[] keywords_arr = line.split("/")[1].split(" "); //the array of keywords
            List<String> keywords = new ArrayList<>(); //keywords list
            for (String keyword : keywords_arr) {
                keywords.add(keyword);
            }
            Topic topic = new Topic(id, name, keywords); //create a topic object
            advertise(topic);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isInt(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (Exception e) {
            System.out.println("it's NOT int");
            return false;
        }
    }

    public void main_method(String args) {
        PubSubAgent agent = new PubSubAgent();
        this.ip = args;
        try {
            clientSocket = new Socket(ip, port);
            input = new DataInputStream(clientSocket.getInputStream());
            assignedPort = input.readInt(); //get the assigned port from the server
            clientSocket.close();
//            clientSocket = new Socket(ip, assignedPort); //set up a connection to the new socket
//           System.out.println("connect to assigned port of " + clientSocket.getPort());
//            in_obj = new ObjectInputStream(clientSocket.getInputStream());
//            topicIDs = (LinkedHashMap<Integer, Topic> )in_obj.readObject();
            typedIn = new BufferedReader(new InputStreamReader(System.in));    
        	System.out.println("Who are you? (Enter 1: publisher, 2: subscriber, 3: exit)");
            line = typedIn.readLine();
            if (line.equals("1")) {
                agent.processPublisher();
            } else if (line.equals("2")) {
                 agent.processSubscriber();
           } else {
                System.out.println("Invalid input");
            }     	
         
            
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    class Receiver implements Runnable {


		@Override
		public void run() {
			try {
		    	receiverPort = 1111;
		    	ObjectInputStream in;
		    	receiverSocket = new ServerSocket(receiverPort);
				serviceSocket = receiverSocket.accept();
				String ip = serviceSocket.getRemoteSocketAddress().toString().replace("/","").split(":")[0];
				System.out.println("In receiver thread ip =  " + ip);
				in = new ObjectInputStream(serviceSocket.getInputStream());
				System.out.println("Before read obj");
				Event event = (Event) in.readObject();
				System.out.println("after read obj");
				System.out.println("event - " + event);
				System.out.println("A new notification received");
				System.out.println("Event ID: " + event.getId());
				System.out.println("Topic: " + event.getTopic().getName());
				System.out.println("Title: " + event.getTitle() );
				System.out.println("Content: " + event.getContent());
			} catch(SocketException e){
				System.out.println("The receiver socket is closed!");
			}
			catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			} ;
			
			
		}
    	
    }
//    
//    private class userInputThread implements Runnable {
//        
//        private PubSubAgent host;
//        
//        public userInputThread(PubSubAgent psa) {
//            this.host = psa;
//        }
//        
//        @Override
//        public void run() {
//            boolean post = host.checkEvent();
//            if(post)
//                host.processUserInput();
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(PubSubAgent.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        
//    }
}
