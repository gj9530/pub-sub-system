package implementation;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//import Users.sanketagarwal.3rd_sem.Distributed.PubSubSystem.src.template;
import template.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * This class handles the passing of messages between edge nodes
 */
public class EventManager {

    // Instance Variables for storing topics and subscribers
    private static LinkedHashMap<Topic, ArrayList<String>> topics; //the keys are topics and the values are the list of pubSubAgents' usernames
    private static LinkedHashMap<String, ArrayList<Topic>> subscribers;//the keys are subscriber's usernames and the values are the list of topics
    private static LinkedHashMap<Integer, Topic> topicIDs; // the keys are topic ids and the values are topic object
    private static LinkedHashMap<String, Topic> topicNames;// the keys are topic names and the values are topic object
    private static LinkedHashMap<String, ArrayList<Event>> sub_eventList;
    private static LinkedHashMap<String, String> subscriber_ip;
    private static ArrayList<EMHelperThread> threadList;
    // private ObjectOutputStream out_obj;
    // private ObjectInputStream in_obj;
    // private DataInputStream in;
    private DataOutputStream out;
    private int threadPoolNum = 100;
    private static int topicIdCounter = 1;
    private static int eventCounter = 1;
    private static int receiverPort = 1111; // The port number for all the client to receive notifications
    // Current Port into
    public static final int MAIN_PORT = 5000;
    public static final int NUM_PORTS = 20;
    //private ArrayList<Integer> ports = null;
    private int ports[] = null;
    private ServerSocket recieveConnectionSocket;
    private Socket socket;

    ExecutorService executor = Executors.newFixedThreadPool(threadPoolNum);

    /*
     * Start the repo service
     * Will be used to construct the EventManager
     */
    public void startService() throws IOException {
        System.out.println("Starting Event Manager Service...");
        topics = new LinkedHashMap<>();
        subscribers = new LinkedHashMap<>();
        topicIDs = new LinkedHashMap<>();
        topicNames = new LinkedHashMap<>();
        sub_eventList = new LinkedHashMap<>();
        subscriber_ip = new LinkedHashMap<>();
        threadList = new ArrayList<>();
        boolean running = true;
        int port_count = NUM_PORTS; //Keeping track of port numbers given to  Pub or Sub
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolNum);
        /*
         * Initializing the port list for Pub and Sub from port number 5001 to 5020
         * Create multiple threads to open all the server sockets.
         */
        ports = new int[NUM_PORTS];
        int port = 5001;
        for (int i = 0; i < NUM_PORTS; i++) {
            ports[i] = port++;
            SocketThread socketThread = new SocketThread(ports[i]);
            executor.execute(socketThread);
            //	Thread t = new Thread(socketThread);
            //	t.start();		
        }

        // Do a loop to keep main thread running.
        try {
            recieveConnectionSocket = new ServerSocket(MAIN_PORT); //create a listening socket.
        } catch (IOException ex) {
            Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (running) {
            try {
                if (port_count == 0) {
                    port_count = NUM_PORTS; //setting back port count to initial size, basically rotation of ports          		
                }
                socket = recieveConnectionSocket.accept();
                out = new DataOutputStream(socket.getOutputStream());
                out.writeInt(ports[port_count - 1]);   //tells the client a new port.
                out.close();
                port_count--;
            } catch (Exception ex) {
                Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
                System.err.println("Error running server: " + ex.getLocalizedMessage());
                ex.printStackTrace();
            }
        }
    }

    /*
     * notify all subscribers of new event 
     */
    private void notifySubscribers(Event event) {
    	String topicName = event.getTopic().getName();
    	ArrayList<String> subs = new ArrayList<>();
    	for(Topic t : topics.keySet()){
    		if(t.getName().equals(topicName)){
    			subs = topics.get(t);
    		}
    	}
    	if(subs == null){
    		subs = new ArrayList<>();
    	}
    	for(String subscriber : subs){
    		String ip = subscriber_ip.get(subscriber);  		
			try {
				Socket s = new Socket(ip,receiverPort);
				ObjectOutputStream sendEvent = new ObjectOutputStream(s.getOutputStream());
				sendEvent.flush();
				sendEvent.writeObject(event);
				sendEvent.flush();
				System.out.println("notification sent");
				s.close();
				
			} catch (IOException e) {
				ArrayList<Event> unnotifiedEvents = new ArrayList<>();
    			if(sub_eventList.containsKey(subscriber)){
    				unnotifiedEvents = sub_eventList.get(subscriber);
    				unnotifiedEvents.add(event);
    				sub_eventList.put(subscriber,unnotifiedEvents);   				
    			} else{
    				unnotifiedEvents.add(event);
    				sub_eventList.put(subscriber, unnotifiedEvents);
    			}
			}			  		
    		
    	}
    	
    }
    
    private boolean  hostAvailability(String ip){
    	try {
			Socket s = new Socket(ip,receiverPort);
			return true;
		}  catch (IOException e) {
			System.out.println("The client at " + ip + " is not online");
		}
    	return false;
    }

    /*
     * add new topic when received advertisement of new topic
     */
    private static void addTopic(Topic topic) {
        topics.put(topic, new ArrayList<String>());
        topicIDs.put(topic.getId(), topic);
        topicNames.put(topic.getName(), topic);
        topicIdCounter++;
    }

    /*
     * add subscriber to the internal list
     */
    private void addSubscriber(String userName) {
        subscribers.put(userName, new ArrayList<>());
    }

    /*
     * remove subscriber from the list
     */
    private void removeSubscriber(String username) {
        ArrayList<Topic> list = subscribers.get(username); //get the list of topics subscribed
        ArrayList<String> agents = new ArrayList<>();
        for (Topic t : list) {
            agents = topics.get(t);
            if (agents.contains(username)) {
                agents.remove(username);
                topics.put(t, agents);
            }
        }
        subscribers.remove(username);
    }

    public static void main_method(String[] args) throws IOException {
        new EventManager().startService();
    }

    //inner thread class to open all the server sockets and connect to the clients at assigned port.
    class SocketThread implements Runnable {

        private int port;
        private ServerSocket ser;
        private Socket socket;
        private boolean running = true;

        SocketThread(int port) {
            this.port = port;
        }

        @Override
        public void run() {
            try {
                ser = new ServerSocket(port);
                while (running) {
                    socket = ser.accept();
                    EMHelperThread helper = new EMHelperThread(socket);
                    threadList.add(helper);
                    executor.execute(helper);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //inner thread class to process the client's request.

    class EMHelperThread extends Thread {

        private Socket socket;
        private String username = "";
        private DataOutputStream output;
        private ObjectOutputStream out_obj;
        private ObjectInputStream in_obj;
        private AgentData agentData;

        EMHelperThread(Socket socket) {
            this.socket = socket;
        }

        private String getUsername() {
            return username;
        }

        private void receiveEvent(Event event) {
            System.out.println(username + " received " + event.getTitle());
        }

        /*
         * This method prints out the current topic list
         */
        private void printTopic(HashMap<Topic, ArrayList<String>> topics) {
            System.out.println("Current topic list:");
            System.out.println("ID" + "\t" + "Name" + "\t" + "Keywords");
            for (Topic topic : topics.keySet()) {
                System.out.print(topic.getId() + "\t" + topic.getName() + "\t");
                for (String keyword : topic.getKeywords()) {
                    System.out.print(keyword + " ");
                }
                System.out.println();
            }
        }

        private void toPublish() {
            try { 
                int eventId = eventCounter++;
                Event event = agentData.getEvent();
                System.out.println("The event = " + event);
                Event newEvent = new Event(eventId,event.getTopic(),event.getTitle(),event.getContent()); //add id to the event
                notifySubscribers(newEvent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void toAdvertise() {
            try {
                Topic topic = agentData.getTopic();
                Topic newTopic = new Topic(topicIdCounter,topic.getName(),topic.getKeywords()); //add topic id to the topic
                addTopic(newTopic);
                printTopic(topics);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void processPublisher() {       
            if (agentData.getRequest() == 1) {
			    toPublish();
			} else if (agentData.getRequest() == 2){
			    toAdvertise();
			}
        
        }

        private void processSubscriber() throws IOException {
        	username = agentData.getUsername();
        	String ip = socket.getRemoteSocketAddress().toString().replace("/","").split(":")[0];
        	System.out.println("the client ip is " + ip);
        	subscriber_ip.put(username,ip);
        	System.out.println("In processSubscriber the agentData request is " + agentData.getRequest());
        	if(!subscribers.containsKey(username)){
        		addSubscriber(username);    		
        	}
            if (agentData.getRequest() == 1) {
                toSubscribe();
            } else if (agentData.getRequest() == 2) {
                toUnsubscribe();
            } else if (agentData.getRequest() == 3) {
            	System.out.println("to subscribe_keyword");
                subscribe_keyword();
            } else if (agentData.getRequest() == 4) {
            	listSubscribedTopics();
            } else if (agentData.getRequest() == 5) {
                removeSubscriber(username);
            } else if (agentData.getRequest() == 6) {
                toShowTopics();
            } 
            else if (agentData.getRequest() == 7) {
                checkEvent();
            } 
         
        }

        private void checkEvent() {
            Boolean canProcess;
            canProcess = sub_eventList.containsKey(username);
            ArrayList<Event> events = new ArrayList<>();
            try {
                if (canProcess) {             
                    events = sub_eventList.get(username);   
                    sub_eventList.remove(username);
                }
                out_obj.writeObject(events);
                out_obj.flush();        
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        private void toShowTopics() throws IOException {
            ArrayList<Topic> topicList = new ArrayList<>(topics.keySet());
            out_obj.writeObject(topicList);
            out_obj.flush();

        }

        private void listSubscribedTopics() throws IOException {
            ArrayList<Topic> subscribedTopics = new ArrayList<>();           
            subscribedTopics = subscribers.get(username);
            if (subscribedTopics == null) {
            	subscribedTopics = new ArrayList<>();
            }
            if (subscribedTopics.size() == 0) {
                System.out.println("No subscription");
            }
            out_obj.writeObject(subscribedTopics);
            out_obj.flush();
        }

        private void toSubscribe() {
            Topic t = agentData.getTopic();
			ArrayList<String> agents = topics.get(t);
			System.out.println("username is " + username);
			if(agents == null){
				agents = new ArrayList<>();
			}
			if (!agents.contains(username)) {
			    agents.add(username);
			    System.out.println(username + " added");
			    topics.put(t, agents);
			}
			ArrayList<Topic> topicList = new ArrayList<>();
			topicList = subscribers.get(username);
			if(topicList == null){
				topicList = new ArrayList<>();
			}
			boolean topicSubscribed = false;
			for (Topic topic : topicList){
				if(topic.getId() == t.getId()){
					topicSubscribed = true;
				}
			}
			if (!topicSubscribed) {
			    topicList.add(t);
			    subscribers.put(username, topicList);
			} else{
				System.out.println("This topic has been subscribed");
			}
        }

        private void subscribe_keyword() {
            String keyword = agentData.getKeywords();
            System.out.println("The keyword is " + keyword);
			if (!topics.isEmpty()) {
			    for (Topic t : topics.keySet()) {
			        if (t.getKeywords().contains(keyword) && !topics.get(t).contains(username)) {
			        	System.out.println("topic id = "  + t.getId() + " contains keyword of " + keyword);
			            ArrayList<String> agents = new ArrayList<>();
			            agents = topics.get(t);
			            agents.add(username);
			            topics.put(t, agents);
			            ArrayList<Topic> tp = new ArrayList<>();
			            tp = subscribers.get(username);
			            if (tp == null){
			            	tp = new ArrayList<>();
			            }
			            
			            boolean topicSubscribed = false;
			            for (Topic topic : tp){
			            	if (topic.getKeywords().equals(keyword)){
			            		topicSubscribed = true;		            		
			            	}
			            }
			            if (!topicSubscribed) {
			                tp.add(t);
			                subscribers.put(username, tp);
			            }
			        } else{System.out.println("keyword of " + keyword + "not contained" );}
			    }
			}
        }

        private void toUnsubscribe() {
            Topic t = agentData.getTopic();
			Boolean canProcess = false;
			if(subscribers.get(username) != null){
				for(Topic topic : subscribers.get(username)){
					if (topic.getId() == t.getId()){
						System.out.println(username + " subscribed " + t.getName());
						canProcess = true;
					}
				}
				
			}
			
			if (!canProcess) {
			    System.out.println("The topic is not subscribed");
			    return;
			}

			ArrayList<String> agents = topics.get(t);
			if(agents == null) {
				agents = new ArrayList<String>();
			}
			if (agents.contains(username)) {
			    agents.remove(username);
			    topics.put(t, agents);
			}
			ArrayList<Topic> topicList = new ArrayList<>();
			topicList = subscribers.get(username);
			if(topicList == null){
				topicList = new ArrayList<>();
			}
			for(Topic topic : topicList){
				if(topic.getId() == t.getId()){
					topicList.remove(topic);
				    subscribers.put(username, topicList);		
				    return;
				}
			}
        }


        public void run() {
            try {
            	out_obj = new ObjectOutputStream(this.socket.getOutputStream());
            	out_obj.flush();
            	out_obj.writeObject(topicIDs);
            	out_obj.flush();
                in_obj = new ObjectInputStream(this.socket.getInputStream());
                agentData = (AgentData) in_obj.readObject();     
                System.out.println("A new agentData received");
                if (agentData.getIdentity() == 1) {
                    System.out.println("The client is a publisher");
                    processPublisher();
                } else if (agentData.getIdentity() == 2) {
                    processSubscriber();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
 
        }
    }
}
