package implementation;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import template.Event;
import template.Publisher;
import template.Subscriber;
import template.Topic;


public class AgentData implements Serializable {
	private int identity,  request, topicId;
	private String username, keywords;
	private Event event;
	private Topic topic;
	
	public void setUsername (String username){
		this.username = username;
	}
	
	public void setIdentity(int identity){
		this.identity = identity;
	}
	
	public void setRequest(int request){
		this.request = request;
	}
	
	public void setKeywords(String keywords){
		this.keywords = keywords;
	}
	
	public void setEvent(Event event){
		this.event = event;
	}
	
	public void setTopic(Topic topic){
		this.topic = topic;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public int getIdentity(){
		return this.identity;
	}
	
	public int getRequest(){
		return this.request;
	}
	
	public String getKeywords(){
		return this.keywords;
	}
	
	public Event getEvent(){
		return this.event;
	}
	
	public Topic getTopic(){
		return this.topic;
	}
	
	
	

}
