# This is a how to guide to get docker containers running for our PubSub Project.
# Aziel Shaw - 02/03/2018

!!!! ASUMPTIONS !!!!
@ You have git installed and have pulled this project.
@ You know how to make a .jar file for this project.
@ You have docker installed. Type the command on the next line to see if it's working.
    'docker version'
@ You have an internet connection. :-)
@ For a list of docker commands you will use and ease of lookup there is a section 
    titled Docker commands.
@ NOTE: Most of my commands require using sudo. You may not require this depending on
    your set up.


!!!! STEPS TO START !!!!
- Clean and Build project into .jar file. 

- Copy JAR file into this directory (The one this file is contained in).
    !!IMPORTANT!! Make sure file is named "PubSub.jar"

- Navigate to project directory /jar in command line.

- Run this command (remove 'sudo' if not on linux/don't require admin priviledges).
    'sudo docker build -t pubsub:latest .'

- To assert that build was successful (May take a while for first time build)
    'sudo docker images'   Should see container you created.

- Run docker container...
    'sudo docker run -i -t pubsub:latest'

- At this point you should be in a new CLI for the container you have just started.

- Run 'ls' command and PubSub.jar should show up.

- Run following command to start application.
    'java -jar PubSub.jar'

- Have fun and enjoy!!! - Upon start up the program will spit out it's current IP
    type 's' to start the server, or c to start the client. Type anything else to quit.


!!!! COMMON DOCKER COMMANDS YOU MAY USE !!!!

docker images
# Lists all the images of docker containers you have on your machines.

docker ps
# Lists all of the current docker containers running on your machines

docker version
# Shows what version of docker you are running. Good to test if you have docker installed.

docker build -t REPO:TAG .
# IMPORTANT COMMAND.  This builds your docker container. The '.' sets the current 
#     directory as where all the files are located that you want to work with.
#     The most important being the DockerFile. Where the docker container is 
#     compiled from. DockerFile will be explained in the next section.
# IMPORTANT NOTES. Replace 'REPO' with the name of the docker container you want.
#     Replace 'TAG' with the tag you want. (Just use 'latest' for best practice)

docker run -i -t REPO:TAG
# Run the docker container with the REPO:TAG you have set.




!!!! DockerFile !!!!


!!!! END OF FILE !!!!
