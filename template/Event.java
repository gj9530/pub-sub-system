package template;

import java.io.Serializable;
import java.util.List;

public class Event implements Serializable {

    private int id;
    private Topic topic;
    private String title;
    private String content;

    public Event(int id, Topic topic, String title, String content) {
        this.id = id;
        this.topic = topic;
        this.title = title;
        this.content = content;
    }

    /*
     * getter method to get id
     */
    public int getId() {
        return this.id;
    }

    /*
     * getter method to get topic
     */
    public Topic getTopic() {
        return topic;
    }

    /*
     * getter method to get title
     */
    public String getTitle() {
        return title;
    }

    /*
     * getter method to get content
     */
    public String getContent() {
        return content;
    }

}
