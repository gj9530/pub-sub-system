package template;

import java.io.Serializable;
import java.util.List;

public class Topic implements Serializable {

    private int id;
    private List<String> keywords;
    private String name;

    public Topic(int id, String name, List<String> keywords) {
        this.id = id;
        this.name = name;
        this.keywords = keywords;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getKeywords() {
        return keywords;
    }
}
